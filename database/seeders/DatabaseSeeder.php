<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\House;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $houses = [
            ['title' => '2 bedroom flat', 'postCode' => 'CF243AR', 'address' => 'Piercefield Place, Cardiff', 'description' => 'TOP FLOOR FLAT l AVAILABLE NOW l OFFERED WITH TWO BEDROOMS OR ONE BEDROOM WITH STUDY ROOM', 'bedrooms' => 2, 'bathrooms' => 2, 'contactNumber' => '07831304350', 'price' => '700'],
            ['title' => '1 bedroom flat', 'postCode' => 'CF243BJ', 'address' => 'Allensbank Road, Cardiff', 'description' => 'One double bedroom first floor rear flat within walking distance to the City Centre. The accommodation comprises of lounge, kitchen with appliances, bathroom plus gas central heating.', 'bedrooms' => 1, 'bathrooms' => 1, 'contactNumber' => '07921304350','price' => '550'],
            ['title' => 'Studio Apartment', 'postCode' => 'CF243AF', 'address' => 'Minister Street, CARDIFF', 'description' => 'This studio flat is located at the top end of CIty Road close to the city centre. The property was recently renovated.', 'bedrooms' => 2, 'bathrooms' => 2, 'contactNumber' => '07831754350','price' => '500'],
            ['title' => '3 bedroom villa', 'postCode' => 'CF241DJ', 'address' => 'Farm Drive, Cyncoed', 'description' => 'We are proud to offer these 2 delightful rooms in a 3 bedroom, 1 bathroom 2 toilet shared house in a great location 1 mile from the centre of Cardiff. Available to move in from 25 March 2021', 'bedrooms' => 3, 'bathrooms' => 2, 'contactNumber' => '07531704350', 'price' => '900'],
            ['title' => '4 bed sharing', 'postCode' => 'CF240EF', 'address' => '184 Cathedral Road, Pontcanna, Cardiff', 'description' => 'WE HAVE 4 ROOMS TO RENT IN A BEAUTIFUL 4 BEDROOM HOUSE AVAILABLE NOW', 'bedrooms' => 1, 'bathrooms' => 1, 'contactNumber' => '07431704950', 'price' => '475'],
        ];

        foreach ($houses as $house){
            House::create($house); 
        }
        dd("Seeded successfully!!");
    }
}
