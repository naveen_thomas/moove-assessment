<?php

namespace Database\Factories;

use App\Models\House;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class HouseFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = House::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            "id"=> 1,
            "title"=> "2 bedroom flat",
            "postCode"=> "CF243AR",
            "address"=> 'Piercefield Place, Cardiff',
            "bedrooms"=> "2",
            "bathrooms"=> "1",
            "description"=> "TOP FLOOR FLAT l AVAILABLE NOW l OFFERED WITH TWO BEDROOMS OR ONE BEDROOM WITH STUDY ROOM",
            "contactNumber"=> "07831804350",
            "price"=> "700",
            "created_at"=> "2021-03-16T15:09:58.000000Z",
            "updated_at"=> "2021-03-17T17:41:49.000000Z"
        ];
    }
}
