<?php

namespace Tests\Unit;

use Tests\TestCase;

class HouseControllerTest extends TestCase
{
    public function test_it_returns_200_status_for_index_method()
    {
        $response = $this->call('GET', 'houses');
        $response->assertStatus(200);
    }
}
