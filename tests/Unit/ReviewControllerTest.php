<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Models\House;
use App\Models\User;

class ReviewControllerTest extends TestCase
{
 
    public function test_it_returns_200_status_for_index_method()
    {
        $house = House::factory()->make();
        $response = $this->call('GET', "houses/{$house->id}/reviews");
        $response->assertStatus(200);
    }

    public function test_it_returns_302_status_for_create_method_when_user_not_authenticated()
    {
        $house = House::factory()->make();
        $response = $this->call('GET', "houses/{$house->id}/reviews/create");
        $response->assertStatus(302);
    }

    public function test_it_returns_200_status_for_create_method_when_user_is_authenticated()
    {
        $user = User::factory()->make();
        $house = House::factory()->make();
        $response = $this->actingAs($user)->call('GET', "houses/{$house->id}/reviews/create");
        $response->assertStatus(200);
    }
}


