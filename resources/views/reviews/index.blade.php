@extends('layouts.app')
@section('content')
    <div class="container">
        <h3>House #{{ $houseId }} Reviews</h3><br>
        @if($reviews->count() == 0)
            <div>No Reviews Posted for this House yet</div>
        @endif
        @foreach ($reviews as $review)
            <div class="one-review">
                <div class="row">
                    <div class="col-md-6">
                        <p class="font-weight-bold">{{$review->user->name}}</p>
                    </div>
                    <div class="col-md-6">
                        Posted On:
                        {{date('d-m-Y', strtotime($review->created_at))}}
                    </div>
                </div>
                <div class="row text-success">
                    <div class="col-md-12">
                        <span class="text-rating">Landlord Rating:</span>
                        @for ($i = 0; $i < $review->landlordRating; $i++)
                          <i class="fa fa-star"></i>
                        @endfor
                    </div>
                </div>
                <div class="row text-success">
                    <div class="col-md-12">
                        <span class="text-rating">House Condition:</span>
                        @for ($i = 0; $i < $review->houseCondition; $i++)
                          <i class="fa fa-star"></i>
                        @endfor
                    </div>
                </div>
                <div class="row text-success">
                    <div class="col-md-12">
                        <span class="text-rating">Overall Experience:</span>
                        @for ($i = 0; $i < $review->overallExperience; $i++)
                          <i class="fa fa-star"></i>
                        @endfor
                    </div>
                </div>
                <div class="row pt-2">
                    <div class="col-md-12">
                        <h6>{{$review->bestAndWorstThing}}</h6>
                        <p>{{$review->comment}}</p>
                    </div>
                </div>
                <div class="row pt-2">
                    <div class="col-md-8">
                        <button type="button" class="btn btn-primary"><i class="fa fa-share-alt"></i> Share</button>

                    </div>
                    <div class="col-md-4">
                        <ul class="list-inline list-unstyled">
                            <li class="list-inline-item">
                                <p>is this helpful to you?</p>
                            </li>
                            <li class="list-inline-item"><button type="button" class="btn btn-outline-success">Yes</button>
                            </li>
                            <li class="list-inline-item"><button type="button" class="btn btn-outline-danger">No</button>
                            </li>
                        </ul>





                    </div>
                </div>
            </div>
            <hr>
        @endforeach
    </div>
@endsection
