@extends('layouts.app')
@section('content')

<div class="container">
    @if(session()->has('error'))
        <div class="alert alert-danger">
            {{ session()->get('error') }}
        </div>
    @endif
    <h3>Please review the house</h3><br>
    <form method="post" action="{{route('houses.reviews.store', [$houseId])}}">
        {{csrf_field()}}
        <div class="form-group">
            <label for="houseCondition">How would you rate the overall condition of the house? </label><br>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="houseCondition" id="inlineRadio1" value="1">
                <label class="form-check-label" for="inlineRadio1">1</label>
            </div>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="houseCondition" id="inlineRadio2" value="2">
                <label class="form-check-label" for="inlineRadio2">2</label>
            </div>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="houseCondition" id="inlineRadio3" value="3">
                <label class="form-check-label" for="inlineRadio3">3</label>
            </div>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="houseCondition" id="inlineRadio4" value="4">
                <label class="form-check-label" for="inlineRadio4">4</label>
            </div>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="houseCondition" id="inlineRadio5" value="5">
                <label class="form-check-label" for="inlineRadio5">5</label>
            </div>
        </div>

        <div class="form-group">
            <label for="landlordRating">What's the landlord like? </label><br>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="landlordRating" id="inlineRadio1" value="1">
                <label class="form-check-label" for="inlineRadio1">1</label>
            </div>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="landlordRating" id="inlineRadio2" value="2">
                <label class="form-check-label" for="inlineRadio2">2</label>
            </div>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="landlordRating" id="inlineRadio3" value="3">
                <label class="form-check-label" for="inlineRadio3">3</label>
            </div>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="landlordRating" id="inlineRadio4" value="4">
                <label class="form-check-label" for="inlineRadio4">4</label>
            </div>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="landlordRating" id="inlineRadio5" value="5">
                <label class="form-check-label" for="inlineRadio5">5</label>
            </div>
        </div>

        <div class="form-group">
            <label for="overallExperience">How was your overall experience? </label><br>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="overallExperience" id="inlineRadio1" value="1">
                <label class="form-check-label" for="inlineRadio1">1</label>
            </div>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="overallExperience" id="inlineRadio2" value="2">
                <label class="form-check-label" for="inlineRadio2">2</label>
            </div>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="overallExperience" id="inlineRadio3" value="3">
                <label class="form-check-label" for="inlineRadio3">3</label>
            </div>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="overallExperience" id="inlineRadio4" value="4">
                <label class="form-check-label" for="inlineRadio4">4</label>
            </div>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="overallExperience" id="inlineRadio5" value="5">
                <label class="form-check-label" for="inlineRadio5">5</label>
            </div>
        </div>

        <div class="form-group">
            <label for="exampleFormControlTextarea1">What's the best & worst thing about the house?</label>
            <textarea name="bestAndWorstThing" class="form-control" id="exampleFormControlTextarea1" rows="2"></textarea>
        </div>

        <div class="form-group">
            <label for="exampleFormControlTextarea1">Any Additional comments you want to say?</label>
            <textarea name="comment" class="form-control" id="exampleFormControlTextarea2" rows="2"></textarea>
        </div>
        <button type="submit" class="btn btn-success">Submit</button>
        <a href="{{route('houses.index')}}" class="btn btn-danger">Back</a>
    </form>

</div>
  @endsection
