@extends('layouts.app')
@section('content')
<div class="container">
    @if(session()->has('error'))
        <div class="alert alert-danger col-md-8">
            {{ session()->get('error') }}
        </div>
    @endif
    @foreach($houses as $house)
        <div class="row mb-3">
            <div class="col-md-8">
                <div class="card shadow">
                    <div class="card-body">
                        <div class="row text-center ">
                            <div class="col-md-8">
                                <h5>{{$house->title}}, {{$house->address}}, {{$house->postCode}}</h5>
                                <p>{{$house->description}}</p>
                                <p>
                                    <i class="fa fa-bed"> x{{$house->bedrooms}}</i>
                                    <i class="icon-bath fa fa-bath"> x{{$house->bathrooms}}</i>
                                </p>
                                <div> Call: {{$house->contactNumber}}</div>
                            </div>
                            <div class="col-md-4">
                                <h3>£{{$house->price}} pcm</h3>
                                <small>An estimated Price*</small>
                                <div class="sub-row">
                                    <a href="{{route('houses.reviews.index', [$house->id])}}" class="btn btn-success btn-sm btn-block">View Reviews</a>
                                    <a href="{{route('houses.reviews.create', [$house->id])}}" class="btn btn-primary btn-sm btn-block">Add Review</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
</div>
@endsection
