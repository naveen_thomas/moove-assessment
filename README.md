# Moove - Take Home Assignment
This is a simple application to view and add reviews for houses.
- All User's are able to see property listings.
- All User's are able to view the reviews of each property.
- Authenticated User is able to add a review for a property.

# Assumptions
User cannot review property again, once it is reviewed earlier by the same user. 

User can review upto 3 properties, after that they cannot review any property.

Used 4 end points in this application:
  - Houses
    - index - To display all properties
  - Reviews
    - index - To display reviews of each property.
    - create - To display the page to add a review for a property.
    - store - To add a review.

## Technology  
### PHP version : 7.3.11
### LARAVEL version : 8.31.0 
### MySQL version : 8.0.23

## Database creation
Create a database in the MySQL server.

Use command **create database _DB_NAME_;** in MySQL(console or workbench)

eg: **create database property_listings** creates a database with the name **property_listings**.

## Database Initialization
Create a replica of the file .env.example in this project and name it as .env and save it.

Add the name of the created database and credentials of MySQL in the following snippet in the file

`
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=property_listings
DB_USERNAME=root
DB_PASSWORD=
` 

Use **php artisan migrate** to run migration

## DB Seed
Use **php artisan db:seed** to populate data in the tables

## Test
Tests are written using PHPUnit and it can be run by using **php artisan test**

## Run project

To run the application,

Firstly use **npm install** to install packages and dependencies

Then, use **php artisan serve** to start the server
