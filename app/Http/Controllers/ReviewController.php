<?php

namespace App\Http\Controllers;

use App\Models\Review;
use App\Models\House;
use Illuminate\Http\Request;
use DB;

class ReviewController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth')->except(['index']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($houseId)
    {
        $house = House::find($houseId);
        $reviews = $house->reviews;

        return view('reviews.index', compact('reviews', 'houseId'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($houseId)
    {
        $user = auth()->user();
        $review = DB::table('reviews')->where('house_id', $houseId)->where('user_id', $user->id)->get();
        if ($review->count() == 1)
          return back()->with('error', 'You are not able to review same property more than once!');
        if ($user->reviews->count() == 3)
          return back()->with('error', 'You are not able to review more than 3 properties!');
        return view('reviews.create', compact('houseId'));
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($houseId, Request $request)
    {
        $user = auth()->user();
        $house = House::find($houseId);
        $house->reviews()->create($request->all() + ['user_id'=> $user->id]);
        return redirect()->route('houses.reviews.index', $houseId);
    }
}
